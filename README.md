# README #

This repo will contain some of my projects made in Python. 
*All code is free to use by all who want to.* I currently go to **IEDP** in Lisbon, Portugal, in a System Management and Programming course.

### What is this repository for? ###

* This project contains some exercises done in Python.

### All code can be used Freely ###

* If you want to use my code go for it.
* If you need help understanding some part of it don't hesitate in messaging me.

## Projects in this build.
[GoodMorning Someone](https://bitbucket.org/iuribdias/psi/src/master/PSI/venv/goodMorning.py).  
[Progress](https://bitbucket.org/iuribdias/psi/src/master/PSI/venv/progressBar.py).		
[addProd](https://bitbucket.org/iuribdias/psi/src/master/PSI/venv/addProd.py).