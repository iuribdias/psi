from tkinter import *
import ttk

# This variable is for keeping track of the position of the progressBar.
stepAmount = 0
# Starting the loop.
root = Tk()


# This function makes the progressBar go forward and checks if the bar has reached the 50% point.
def plus():
    progressbar.step(5)
    global stepAmount
    stepAmount += 1
    if stepAmount > 10:
        progressbar.step(-5)
        stepAmount -= 1


# This function makes the progressBar go backwards.
def minus():
    progressbar.step(-5)
    global stepAmount
    stepAmount -= 1


# Defining the progressBar.
progressbar = ttk.Progressbar(orient=HORIZONTAL, length=200, mode='determinate')
progressbar.pack(side="bottom")

# Defining buttons.
c = Button(root, text="Minus", command=minus)
c.pack(side="left")
b = Button(root, text="Plus", command=plus)
b.pack(side="right")
b.pack()

# Ending the loop.
root.mainloop()
