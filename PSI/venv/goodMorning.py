from tkinter import *
import datetime

# Defining the datetime variable.
now = datetime.datetime.now()
# Starting the loop.
root = Tk()

# Defining the strings.
nome = StringVar()
final = StringVar()


# This is the function that handles the click.
def bGo():
    if now.hour > 12:
        final.set(nome.get() + " " + "Buenas Noches")

    else:
        final.set(nome.get() + " " + "Buenos Dias")


# Defining all of the UI elements.
tbName = Entry(root, textvariable=nome)
tbName.pack(side="top")
bUno = Button(text="Go", command=bGo)
bUno.pack(side="right")
lRes = Label(root, textvariable=final)
lRes.pack(side="bottom")

# Ending the loop.
root.mainloop()
