from tkinter import *

root = Tk()


def tbd():
    lbUno.insert(0, tbText.get())


window = Toplevel(root)
tbText = Entry(window)
bAdd = Button(window, text="Adicionar", command=tbd)
bClose = Button(window, text="Fechar")


def create_window():
    tbText.pack(side="top")
    bAdd.pack(side="left")
    bClose.pack(side="right")


bAddProd = Button(root, text="Adicionar Produtos", command=create_window)
bAddProd.pack(side="left")
lbUno = Listbox(root)
lbUno.pack(side="bottom")

root.mainloop()
